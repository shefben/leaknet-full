$keywordsDir = "u:/keywords";
$baseMatDir = "u:/hl2/hl2/materials";

use Win32::Shortcut;

sub ResolveShortcut
{
	local( $shortcut ) = shift;
	local( $link ) = Win32::Shortcut->new( $shortcut );
	return $link->{'Path'};
}

sub ProcessVMTFile
{
	local( $keyword ) = shift;
	local( $name ) = shift;
#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
		return;
	}

	local( $shortcut ) = $keywordsDir . "/" . $keyword . "/" . $name;
	local( $vmt ) = &ResolveShortcut( $shortcut ) . "\n";

	print "vmt: $vmt keyword: $keyword\n";

	local( $foundKeywords ) = 0;
	local( @fileContents );

	local( *VMT );
	open VMT, "<$vmt";
	local( $line );
	while( $line = <VMT> )
	{
		$line =~ s/materialset/keywords/gi;
		if( $line =~ m/\s*^\$keywords\s+=\s+\".*\"/i )
		{
			if( !$foundKeywords )
			{
				$foundKeywords = 1;
				#found keywords. . check if the one that we care about is in there.
				if( $line =~ m/$keyword/i )
				{
					# The keyword is already there, leave it alone.
				}
				else
				{
					$line =~ m/\s*^\$keywords\s+=\s+\"(.*)\"/i;
					if( length $line )
					{
						$line = "\$keywords = \"$1,$keyword\"\n";
					}
					else
					{
						$line = "\$keywords = \"$keyword\"\n";
					}
				}
			}
		}
		push @fileContents, $line;
	}
	if( $foundKeywords == 0 )
	{
		push @fileContents, "\$keywords = \"$keyword\"\n";
	}
	close VMT;

	open VMT, ">$vmt";
	print VMT @fileContents;
	close VMT;

}

sub ProcessKeywordSet
{
	local( $name ) = shift;
#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
		return;
	}
	local( $keyword ) = $name;
	$name = $keywordsDir . "/" . $name;
	if( -d $name )
	{
		local( *SRCDIR );
		opendir SRCDIR, $name;
		local( @dir ) = readdir SRCDIR;
		closedir SRCDIR;

		local( $item );
		while( $item = shift @dir )
		{
			&ProcessVMTFile( $keyword, $item );
		}
	}
}

sub ProcessKeywordsDir
{
	local( $name ) = shift;

#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
		return;
	}

#   Figure out if it's a file or a directory.
	if( -d $name )
	{
		local( *SRCDIR );
#		print "$name is a directory\n";
		opendir SRCDIR, $name;
		local( @dir ) = readdir SRCDIR;
		closedir SRCDIR;

		local( $item );
		while( $item = shift @dir )
		{
			&ProcessKeywordSet( $item );
		}
	}
}

sub RemoveKeywordsFromVMTFile
{
	local( $vmt ) = shift;

	local( @fileContents );

	local( *VMT );
	open VMT, "<$vmt";
	local( $line );
	while( $line = <VMT> )
	{
		if( $line =~ m/keywords/i || $line =~ m/materialset/i )
		{
			next;
		}
		push @fileContents, $line;
	}
	close VMT;

	open VMT, ">$vmt";
	print VMT @fileContents;
	close VMT;
}

sub RemoveKeywordsFromAllVMTs
{
	local( $name ) = shift;

#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
		return;
	}

#   Figure out if it's a file or a directory.
	if( -d $name )
	{
		local( *SRCDIR );
#		print "$name is a directory\n";
		opendir SRCDIR, $name;
		local( @dir ) = readdir SRCDIR;
		closedir SRCDIR;

		local( $item );
		while( $item = shift @dir )
		{
			&RemoveKeywordsFromAllVMTs( $name . "/" . $item );
		}
	}
	elsif( -f $name )
	{
		if( $name =~ /\.vmt/i )
		{
			&RemoveKeywordsFromVMTFile( $name );
		}
	}
	else
	{
		print "$name is neither a file or a directory\n";
	}
}

&RemoveKeywordsFromAllVMTs( $baseMatDir );
&ProcessKeywordsDir( $keywordsDir );

