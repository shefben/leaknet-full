//================ Copyright � 2021, VXP, All rights reserved. ================
//
// Purpose: TeamFortress 2 game definition file (.fgd) 
//
//=============================================================================


@include "base.fgd"



// Common things goes here
@BaseClass base(Targetname, Parentname, Angles) = TF2BaseObject
[
	// From CBaseCombatCharacter
	Relationship(string) : "Relationship" : ""
	spawnflags(Flags) = // VXP: Maybe not?
	[
		1 : "Wait Till Seen" : 0
		2 : "Gag" : 0
		4 : "Fall to ground" : 1
		8 : "Drop Healthkit" : 0
		16 : "Efficient" : 0
		128: "Wait For Script" : 0
		256: "Long Visibility/Shoot" : 0
		512: "Fade Corpse" : 0
		1024: "Think outside PVS" : 0
		2048: "Template NPC (used by npc_maker, will not spawn)" : 0
	]

	// From CBaseObject
	Invulnerable(choices) : "Is invulnerable" : 1 =
	[
		0 : "No"
		1 : "Yes"
	]
	RepairMult(float) : "Repair Rate" : 1
	AttackNotify(string) : "Sound when getting attacked" : "" // VXP: TODO: Default attack sound
	MinDisabledHealth(float) : "Minimum Disable Health" : 0 : "If non-zero then if health gets below this amount, the object becomes disabled"
	SolidToPlayer(choices) : "Is solid to player" : 0 =
	[
		0 : "Default"
		1 : "Solid to player"
		2 : "Not solid to player"
	]
	DisabledModel(string) : "Disabled Model" : "" : "If not empty, then when going disabled, switch to this model"
	CantDie(choices) : "Can't die" : 0 =
	[
		0 : "No"
		1 : "Yes"
	]

	input SetHealth(integer) : "Sets a new value for health. If the object's health reaches zero it will break."
	input AddHealth(integer) : "Adds health to the object. If the object's health reaches zero it will break."
	input RemoveHealth(integer) : "Removes health from the object. If the object's health reaches zero it will break."
	input SetMinDisabledHealth(float) : "Sets a minimum health, that will indicate that this object is disabled."
	input SetSolidToPlayer(integer) : "Sets if a player is solid to this object."

	output OnDestroyed(void) : "Fires when the object has been destroyed."
	output OnDamaged(void) : "Fires when the object has been damaged."
	output OnRepaired(void) : "Fires when the object has been repaired."
	output OnDisabled(void) : "Fires when the object has been disabled."
	output OnReenabled(void) : "Fires when the object has been reenabled."
	output OnObjectHealthChanged(void) : "Fires when object's health has been changed."
]



@SolidClass base(Trigger) = trigger_resourcezone :
	"Resource collection area."
[
	target(target_destination) : "Target gather point (info_target?)" : ""
	ResourceAmount(integer) : "Resource Amount" : 10000
	ResourceChunks(integer) : "Resource Chunks amount" : 5
	ResourceRate(float) : "Time between each suck from this zone by resource pumps" : 15
	ChunkValueMin(float) : "" : 20
	ChunkValueMax(float) : "" : 60

	input SetAmount(integer) : "Sets the amount."
	input ResetAmount(void) : "Resets the amount."
	input SetActive(void) : "Activate."
	input SetInactive(void) : "Deactivate."
	input ToggleActive(void) : "Toggle between activated and deactivated."

	output OnEmpty(void) : "Fires when the Resource Zone is empty."
]

@SolidClass base(Trigger) = trigger_controlzone :
	"Resource Zone trigger."
[
	LockAfterChange(integer) : "Auto-lock after the control zone changes hands through combat" : 0
	UncontestedTime(float) : "Time that the control zone has to be uncontested for it to succesfully change teams" : 5
	ContestedTime(float) : "Time that the control zone has to be contested for for it to change to Contested mode (no team)" : 10
	ZoneNumber(integer) : "Zone Number (1-32)" : 0 // VXP: TODO: choices?

	input SetTeam(void) : "Sets the controlling team to the activator's team."
	input LockTeam(void) : "Set it so the team can no longer change, until a set controlling team action occurs."

	output ControllingTeam(void) : "Outputs the team currently controlling this spot, whenever it changes - this is -1 when contended."
]

@SolidClass base(Trigger) = trigger_fall :
	"Used at the bottom of maps where objects should fall away to infinity."
[
	output OnFallingObject(void) : "Fires when object or player passes through this trigger."
]

@SolidClass base(Trigger) = trigger_skybox2world :
	"3DSkybox to World Transition Trigger (useless, since not implemented)."
[
]

@SolidClass base(Parentname, Origin) = func_construction_yard :
	"Place where we can build vehicles" // VXP: TODO: ???
[
	input SetActive(void) : "Activates the Construction Yard."
	input SetInactive(void) : "Deactivates the Construction Yard."
	input ToggleActive(void) : "Toggle between activated and deactivated."
]

@SolidClass base(Parentname, Origin) = func_mass_teleport :
	"Mass Teleport"
[
	MCV(choices) : "Is it for use by MCVs in the field" : 0 =
	[
		0 : "No"
		1 : "Yes"
	]

	input DoTeleport(void) : "Teleport."

	output OnTeleport(void) : "Fires when teleported."
	output OnActive(void) : "Fires when activated (at least 1 MCV)."
	output OnInactive(void) : "Fires when deactivated (no MCVs)."
]

@SolidClass base(Parentname, Origin) = func_no_build :
	"Place where you can't build certain items."
[
	OnlyPreventTowers(choices) : "Only prevent towers" : 0 =
	[
		0 : "No"
		1 : "Yes"
	]

	input SetActive(void) : "Activate."
	input SetInactive(void) : "Deactivate."
	input ToggleActive(void) : "Toggle."
]

@SolidClass base(Door, Origin) = func_door_weldable :
	"Func door that's weldable shut"
[
	movedir(angle) : "Move Direction (Pitch Yaw Roll)" : "0 0 0" : "The direction the door will move when it opens."
	weldpoints(string) : "Weld points" : ""
]




@PointClass base(Targetname, Parentname) size(-16 -16 -16, 16 16 16) = env_fallingrocks : "A falling rock spawner entity" 
[
	FallSpeed(float) : "Fall strength" : 100
	RotationSpeed(float) : "Speed of rotation around Z axis" : 20
	MinSpawnTime(float) : "Minimum spawn time" : 10
	MaxSpawnTime(float) : "Maximum spawn time" : 50
]

@PointClass base(EnableDisable, Targetname, Parentname, Angles) = env_meteorspawner : "Meteor spawner" 
[
	MeteorType(integer) : "Type of meteor (not being used)" : 0
	SpawnInSkybox(choices) : "Spawn in skybox (ShouldTransmit)" : 1 =
	[
		0 : "No"
		1 : "Yes"
	]
	SpawnIntervalMin(float) : "Spawn time - Min" : 5
	SpawnIntervalMax(float) : "Spawn time - Max" : 10
	SpawnCountMin(integer) : "Number of meteors to spawn - Max" : 2
	SpawnCountMax(integer) : "Number of meteors to spawn - Max" : 3
	MeteorSpeedMin(float) : "Meteor speed - Min" : 50
	MeteorSpeedMax(float) : "Meteor speed - Max" : 60
	MeteorDamageRadius(float) : "Meteor damage radius" : 40

]

@PointClass base(Targetname, Parentname) = env_meteortarget : "Meteor target" 
[
	EffectRadius(float) : "Effect radius" : 5
]

@PointClass base(Targetname, Parentname, Angles) = env_shootingstarspawner : "Shooting Star spawner" 
[
	SpawnInterval(float) : "How often do I spawn shooting stars" : 30
	SpawnInSkybox(choices) : "Spawn in skybox" : 1 =
	[
		0 : "No"
		1 : "Yes"
	]
]

@PointClass base(Targetname, Parentname) = env_resourcespawner :
	"A resource chunk spawning point" 
[
	target(target_destination) : "Target Resource Zone" : ""

]

@PointClass base( Targetname, Parentname, Angles ) size( -8 -8 -8, 8 8 8 ) = info_act :
	"Map entity that defines an act."
[
	spawnflags(flags) =
	[
		1 : "Act is an intermission" : 0
		2 : "Waiting for the game start" : 0
	]

	ActNumber(integer) : "Act number" : 0
	ActTimeLimit(float) : "Act time limit (0 - no limit)" : 0
	IntermissionCamera(target_destination) : "Camera point name"
	Respawn1Team1Time(integer) : "Respawn1Team1Time" : 0
	Respawn2Team1Time(integer) : "Respawn2Team1Time" : 0
	Respawn1Team2Time(integer) : "Respawn1Team2Time" : 0
	Respawn2Team2Time(integer) : "Respawn2Team2Time" : 0
	RespawnTeam1InitialDelay(integer) : "RespawnTeam1InitialDelay" : 0
	RespawnTeam2InitialDelay(integer) : "RespawnTeam2InitialDelay" : 0

	input Start(void) : "Force the act to start."
	input FinishWinNone(void) : "Force the act to finish, with no team as the winners."
	input FinishWin1(void) : "Force the act to finish, with team 1 as the winners."
	input FinishWin2(void) : "Force the act to finish, with team 2 as the winners."
	input AddTime(float) : "Add time to the act's time."

	output OnStarted(void) : "The act has started."
	output OnFinishedWinNone(void) : "The act has finished, with no team as the winners."
	output OnFinishedWin1(void) : "The act has finished, with team 1 as the winners."
	output OnFinishedWin2(void) : "The act has finished, with team 2 as the winners."
	output OnTimerExpired(void) : "Time limit has been reached."

	output OnRespawn1Team1_90sec(void) : "Fires when 90 seconds is left for team 1 on respawn 1."
	output OnRespawn1Team1_60sec(void) : "Fires when 60 seconds is left for team 1 on respawn 1."
	output OnRespawn1Team1_45sec(void) : "Fires when 45 seconds is left for team 1 on respawn 1."
	output OnRespawn1Team1_30sec(void) : "Fires when 30 seconds is left for team 1 on respawn 1."
	output OnRespawn1Team1_10sec(void) : "Fires when 10 seconds is left for team 1 on respawn 1."
	output OnRespawn1Team1(void) : "Fires when 0 seconds is left for team 1 on respawn 1."
	output Respawn1Team1TimeRemaining(void) : "Output."

	output OnRespawn2Team1_90sec(void) : "Fires when 90 seconds is left for team 1 on respawn 2."
	output OnRespawn2Team1_60sec(void) : "Fires when 60 seconds is left for team 1 on respawn 2."
	output OnRespawn2Team1_45sec(void) : "Fires when 45 seconds is left for team 1 on respawn 2."
	output OnRespawn2Team1_30sec(void) : "Fires when 30 seconds is left for team 1 on respawn 2."
	output OnRespawn2Team1_10sec(void) : "Fires when 10 seconds is left for team 1 on respawn 2."
	output OnRespawn2Team1(void) : "Fires when 0 seconds is left for team 1 on respawn 2."
	output Respawn2Team1TimeRemaining(void) : "Output."

	output OnRespawn1Team2_90sec(void) : "Fires when 90 seconds is left for team 2 on respawn 1."
	output OnRespawn1Team2_60sec(void) : "Fires when 60 seconds is left for team 2 on respawn 1."
	output OnRespawn1Team2_45sec(void) : "Fires when 45 seconds is left for team 2 on respawn 1."
	output OnRespawn1Team2_30sec(void) : "Fires when 30 seconds is left for team 2 on respawn 1."
	output OnRespawn1Team2_10sec(void) : "Fires when 10 seconds is left for team 2 on respawn 1."
	output OnRespawn1Team2(void) : "Fires when 0 seconds is left for team 2 on respawn 1."
	output Respawn1Team2TimeRemaining(void) : "Output."

	output OnRespawn2Team2_90sec(void) : "Fires when 90 seconds is left for team 2 on respawn 2."
	output OnRespawn2Team2_60sec(void) : "Fires when 60 seconds is left for team 2 on respawn 2."
	output OnRespawn2Team2_45sec(void) : "Fires when 45 seconds is left for team 2 on respawn 2."
	output OnRespawn2Team2_30sec(void) : "Fires when 30 seconds is left for team 2 on respawn 2."
	output OnRespawn2Team2_10sec(void) : "Fires when 10 seconds is left for team 2 on respawn 2."
	output OnRespawn2Team2(void) : "Fires when 0 seconds is left for team 2 on respawn 2."
	output Respawn2Team2TimeRemaining(void) : "Output."

	output OnTeam1RespawnDelayDone(void) : "Output."
	output OnTeam2RespawnDelayDone(void) : "Output."
]

@PointClass base( Targetname, Parentname, Angles ) = info_add_resources :
	"Map entity that gives resources to players passed into it."
[
	ResourceAmount(integer) : "Amount of resources" : 1000

	input Player(string) : "Adds an amount of resources to the given player."

	output OnAdded(void) : "Fires when entity tries to add resources."
]

@PointClass base( Targetname, Parentname, Angles ) = info_buildpoint :
	"Map entity that allows players to build objects on it."
[
	spawnflags(flags) =
	[
		1 : "Allow all manned guns to be built on this point" : 0
		2 : "Allow all vehicles to be built on this point" : 0
	]

	AllowedObject(string) : "Allowed object" : ""
	// VXP: TODO
//	AllowedObject(choices) : "Allowed object" : "" =
//	[
//		"OBJ_POWERPACK" : "Power Packs"
//		"OBJ_RESUPPLY" : "Resupplies"
//		... And so on
//	]
]

@PointClass base( Targetname, Parentname, Angles ) = info_customtech :
	"Map entity that adds a custom technology to the techtree."
[
	TechToWatch(string) : "Tech name" : ""
	NewTechFile(string) : "Tech file" : "scripts/technologytree.txt"


	output TechPercentage(void) : "Percentage of the tech that's owned."
]

@PointClass base( Targetname, Parentname, Angles ) = info_input_playsound :
	"Map entity that plays sounds to players."
[
	spawnflags(flags) =
	[
		1 : "Play from this entity's location" : 1
	]

	Sound(sound) : "Sound" : ""
	Volume(float) : "Volume" : 10
	Attenuation(float) : "Attenuation" : "0.8" // VXP: TODO: choices?
	TestVolume(target_destination) : "Test volume (must be a trigger)" : ""

	input SetSound(string) : "Set the sound to play."
	input PlaySoundToAll(void) : "Play sound to all players."
	input PlaySoundToTeam1(void) : "Play sound to all players on team 1."
	input PlaySoundToTeam2(void) : "Play sound to all players on team 2."
	input PlaySoundToPlayer(string) : "Play sound to a specific player."
]

@PointClass base( Targetname, Parentname, Angles ) = info_input_resetbanks :
	"Map entity that resets player's banks."
[
	ResetAmount(integer) : "Reset amount" : 100

	input SetResetAmount(integer) : "Set the reset amount."
	input ResetAll(void) : "Reset all the player's resource banks."
	input ResetTeam1(void) : "Reset all of team 1's player's resource banks."
	input ResetTeam2(void) : "Reset all of team 2's player's resource banks."
	input ResetPlayer(string) : "Reset a specific player's resource banks."
]

@PointClass base( Targetname, Parentname, Angles ) = info_input_resetobjects :
	"Map entity that resets player's objects."
[
	input ResetAll(void) : "Reset all the player's resource objects."
	input ResetTeam1(void) : "Reset all of team 1's player's resource objects."
	input ResetTeam2(void) : "Reset all of team 2's player's resource objects."
	input ResetPlayer(string) : "Reset a specific player's resource objects."
]

@PointClass base( Targetname, Parentname, Angles ) = info_input_respawnplayers :
	"Map entity that respawns players."
[
	input RespawnAll(void) : "Respawn all the players."
	input RespawnTeam1(void) : "Respawn all of team 1's players."
	input RespawnTeam2(void) : "Respawn all of team 2's players."
	input RespawnPlayer(string) : "Respawn a specific player."
]

@PointClass base( Targetname, Parentname, Angles ) = info_minimappulse :
	"Map entity that makes a pulse on the minimap."
[
	input PulseForAll(void) : "Pulse for all the players."
	input PulseForTeam1(void) : "Pulse for all of team 1's players."
	input PulseForTeam2(void) : "Pulse for all of team 2's players."
	input PulseForPlayer(string) : "Pulse for a specific player."
]

@PointClass base( Targetname, Parentname, Angles ) = info_output_team :
	"Map entity that fires its output with all the players in a team."
[
	input Fire(void) : "Loop through all the players on the team and fire our output with each of them."

	output Player(void) : "Output."
]

@PointClass base( Targetname, Parentname, Angles ) = info_vehicle_bay :
	"Vehicle build point."
[
]

@PointClass base( Targetname, Parentname, Angles ) = info_weldpoint :
	"A weld point for func_door_weldable."
[
	target(target_destination) : "Endpoint for this weld point" : ""
]

@PointClass base(vgui_screen_base) size(-4 -4 -4, 4 4 4) = vgui_screen_vehicle_bay :
	"Vehicle Bay VGui Screen" 
[
	target(target_destination) : "Target Vehicle Bay (buildpoint)" : ""


	output OnStartedBuild(void) : "Fires when launched building."
	output OnFinishedBuild(void) : "Fires when finished building."
	output OnReadyToBuildAgain(void) : "Fires when bay is clear enough to allow another vehicle to be built on it."
]




// CObjectTunnel
// CWalkerStrider



@PointClass base(EnableDisable, Angles) studio("models/editor/playerstart.mdl") = info_player_teamspawn :
	"TF2 Team Player Spawn"
[
	TeamNum(choices) : "Team" : 1 = 
	[
		1: "Humans"
		2: "Aliens"
	]

	output OnPlayerSpawn(void) : "Fires when the player has spawned at spawnpoint."
]
@PointClass base(EnableDisable, Angles) studio("models/editor/playerstart.mdl") = info_vehicle_groundspawn :
	"TF2 Team Vehicle Spawn"
[
	TeamNum(choices) : "Team" : 1 = 
	[
		1: "Humans"
		2: "Aliens"
	]

//	output OnPlayerSpawn(void) : "Fires when the player has spawned at spawnpoint."
	output OnVehicleSpawn(void) : "Fires when the vehicle has spawned at spawnpoint."
]



@PointClass iconsprite("editor/logic_auto_TODO.vmt") = sensor_tf_team :
	"Detects a bunch of tf team state."
[
	team(choices) : "Team number" : 0 =
	[
		0 : "Undefined"
		1 : "Humans"
		2 : "Aliens"
	]

	output OnRespawnCountChanged(void) : "Fires when respawn count changed, passes the count."
	output OnResourceCountChanged(void) : "Fires when resource count changed, passes the count."
	output OnMemberCountChanged(void) : "Fires when member count changed, passes the count."
	output OnRespawnCountChangedDelta(void) : "Fires when respawn count changed, passes the difference between the old count and new."
	output OnResourceCountChangedDelta(void) : "Fires when resource count changed, passes the difference between the old count and new."
	output OnMemberCountChangedDelta(void) : "Fires when member count changed, passes the difference between the old count and new."
]



// VXP: Both of them are linked to one class, and the model is based on team number
@PointClass base(BaseNPC) studio() = cycler_tf2commando : "Human Commando cycler"
[
	input RaiseShield(void) : "Input that raises the cycler's shield."
	input LowerShield(void) : "Input that lowers the cycler's shield."
]
@PointClass base(BaseNPC) studio() = cycler_aliencommando : "Alien Commando cycler"
[
	input RaiseShield(void) : "Input that raises the cycler's shield."
	input LowerShield(void) : "Input that lowers the cycler's shield."
]



@PointClass base(BaseNPCMaker, Angles, DXLevelChoice) size(-16 -16 -16, 16 16 16) color(0 0 255) = npc_bughole : "Bug Hole"
[
	PoolSize(integer) : "Pool size" : 100
	PoolRegen(float) : "Pool regeneration time" : 20
	PatrolTime(float) : "Interval between patrols" : 15
	PatrolName(target_destination) : "Name of patrol hint (HINT_BUG_PATROL_POINT)" : ""
	MaxPatrollers(integer) : "Maximum patrol bugs" : 20
	MaxBuilders(integer) : "Maximum builder bugs" : 10
]



@FilterClass base(BaseFilter) size(-8 -8 -8, 8 8 8) = filter_act :
	"Filter that filters by act number."
[
	filteract(integer) : "Filter Act number"
]


// VXP: FIXME: Does these obj_'s even needed to be in Worldcraft?
//@PointClass base(TF2BaseObject) studio("models/objects/obj_aerial_sentry_station.mdl") = obj_armor_upgrade :
//	"This class shows a temporary model until you get to an object it can upgrade.\n"+
//	"Then it draws a shell over the model you want to upgrade."
//[
//]

@PointClass base(TF2BaseObject) studio("models/objects/obj_barbed_wire.mdl") = obj_barbed_wire :
	"Barbed wire."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/human_obj_buffstation.mdl") = obj_buff_station :
	"Medic's portable power generator. Previously obj_portable_power_generator."
[
	input PlayerSpawned(void) : "Input."
	input PlayerAttachedToGenerator(void) : "Input."
	input PlayerEnteredVehicle(void) : "Input."
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_bunker.mdl") = obj_bunker :
	"A stationary bunker that players can take cover in."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_bunker_ladder.mdl") = obj_bunker_ladder :
	"Bunker ladder."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/human_obj_dragonsteeth.mdl") = obj_dragonsteeth :
	"Object built to block vehicle movement."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_antimortar.mdl") = obj_empgenerator :
	"EMP Generator Combat Object."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_explosives.mdl") = obj_explosives :
	"Upgrade that explodes when it's object dies, injuring nearby enemies."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/human_obj_manned_rocketlauncher.mdl") = obj_manned_missilelauncher :
	"A stationary gun that players can man that's built by the player."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_manned_plasmagun.mdl") = obj_manned_shield :
	"A stationary gun that players can man that's built by the player."
[
]

@BaseClass base(TF2BaseObject) = TF2ObjectMapDefined
[
	spawnflags(flags) =
	[
		1 : "Suppress minimap pulse" : 0
		2 : "Suppress attack notify" : 0
		4 : "Doesn't need power" : 0
	]

	health(integer) : "Health" : 100
	CustomName(string) : "Custom Name"
]

// VXP: FIXME: What are these two even doing here?
@SolidClass base(TF2ObjectMapDefined) = func_obj_mapdefined :
	"Map Defined object placed by mapmakers. Is it empty?"
[
]
@PointClass base(TF2ObjectMapDefined) = obj_mapdefined :
	"Map Defined object placed by mapmakers. Is it empty?"
[
	model(studio) : "Model"
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_resupply.mdl") = obj_mcv_selection_panel :
	"A panel which connects to teleport station?"
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_mortar.mdl") = obj_mortar :
	"Indirect's mortar object."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/human_obj_powerpack.mdl") = obj_powerpack :
	"Human's power pack."
[
]

@PointClass base(TF2BaseObject) studio("models/props/common/holo_banner/holo_banner.mdl") = obj_rallyflag :
	"Rally flag that's built by players. Could power up RUSH nearby players."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_resourcepump.mdl") = obj_resourcepump :
	"Resource pump. Should be planted at the Resource Zone (trigger_resourcezone)."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_respawn_station.mdl") = obj_respawn_station :
	"Portable respawn station."
[
	InitialSpawn(integer) : "Initial spawn"
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_resupply.mdl") = obj_resupply :
	"Medic's resupply beacon. Resupply object that's built by the player.\n" +
	"Could be placed on wall or ground."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_sandbag_bunker.mdl") = obj_sandbag_bunker :
	"A stationary sandbag bunker that players can take cover in."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_selfheal.mdl") = obj_selfheal :
	"Upgrade that heals the object over time."
[
]

@PointClass base(TF2BaseObject) studio("models/sentry2.mdl") = obj_sentrygun_plasma :
	"Defender's sentrygun object, that shoots plasma."
[
]
@PointClass base(TF2BaseObject) studio("models/sentry3.mdl") = obj_sentrygun_rocketlauncher :
	"Defender's sentrygun object, that shoots rockets."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_shieldwall.mdl") = obj_shieldwall :
	"Shield wall object that's built by the player."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_tower.mdl") = obj_tower :
	"A stationary tower that players can take cover in."
[
]
@PointClass base(TF2BaseObject) studio("models/objects/obj_tower_ladder.mdl") = obj_tower_ladder :
	"A ladder for the Tower."
[
]

// VXP: FIXME: Should it be SolidClass?
@SolidClass base(TF2ObjectMapDefined) = obj_tunnel :
	"Tunnel."
[
]

@PointClass base(TF2ObjectMapDefined) = obj_tunnel_prop :
	"Tunnel."
[
	model(studio) : "Model"
]
@PointClass base( Targetname, Parentname, Angles ) = info_tunnel_exit :
	"Tunnel exit point."
[
]
@SolidClass base(Trigger) = obj_tunnel_trigger :
	"Tunnel trigger."
[
	TeleportDuration(float) : "Teleport Duration" : "-1.0"
	TeleportVelocity(float) : "Teleport Velocity" : 0

	input SetActive(void) : "Activate."
	input SetInactive(void) : "Deactivate."
	input ToggleActive(void) : "Toggle between activated and deactivated."
	input SetTarget(string) : "Sets the target for tunnel trigger to teleport to."
	input SetTeleportDuration(float) : "Sets the teleport duration."
	input SetTeleportVelocity(float) : "Sets the teleport velocity."

	output OnTunnelTriggerStart(void) : "Fires when tunnel trigger starts teleporting."
	output OnTunnelTriggerEnd(void) : "Fires when the player has been teleported via tunnel trigger."
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_vehicle_boost.mdl") = obj_vehicle_boost :
	"Upgrade that boosts vehicle speeds for short periods of time."
[
]

@PointClass base(TF2BaseObject) studio("models/editor/playerstart.mdl") = player :
	"TF2's player object."
[
	input Respawn(void) : "Input handler that forces a respawn of the player."
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_battering_ram.mdl") = vehicle_battering_ram :
	"A moving vehicle that is used as a battering ram."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/obj_flatbed.mdl") = vehicle_flatbed :
	"Flatbed truck that can carry multiple stationary objects."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_mortar.mdl") = vehicle_mortar :
	"A moving vehicle that is used as a battering ram."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_motorcycle.mdl") = vehicle_motorcycle :
	"A motorcycle."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_motorcycle.mdl") = vehicle_motorcycle :
	"A motorcycle."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_siege_tower.mdl") = vehicle_siege_tower :
	"A Siege Tower Vehicle."
[
]
@PointClass base(TF2BaseObject) studio("models/objects/vehicle_siege_ladder.mdl") = obj_siege_ladder :
	"A Siege Ladder (Object)."
[
]
@PointClass base(TF2BaseObject) studio("models/objects/vehicle_siege_plat.mdl") = obj_siege_platform :
	"A Siege Platform (Object)."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_tank.mdl") = vehicle_tank :
	"A moving vehicle that is used as a battering ram."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_teleport_station.mdl") = vehicle_teleport_station :
	"A Teleport Station vehicle."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/vehicle_wagon.mdl") = vehicle_wagon :
	"A moving vehicle that is used as a battering ram."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/alien_vehicle_strider.mdl") = walker_mini_strider :
	"A mini-strider that player can shoot from."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/alien_vehicle_strider.mdl") = walker_mini_strider :
	"A mini-strider that player can shoot from."
[
]

@PointClass base(TF2BaseObject) studio("models/objects/walker_strider.mdl") = walker_strider :
	"A strider that player can shoot from."
[
]

// obj_driver_machinegun

// obj_manned_plasmagun

// shield_mobile



//-------------------------------------------------------------------------
//
// Weapons
//
//-------------------------------------------------------------------------
@BaseClass color(0 0 200) base(Targetname, Angles) = Weapon
[
	spawnflags(Flags) =
	[
		1 : "Start constrained" : 0
	]

	output OnPlayerPickup(void) : "Fires when the player picks up this weapon"
	output OnNPCPickup(void) : "Fires when an NPC picks up this weapon"
]

@PointClass base(Weapon) studio("models/weapons/w_arcwelder.mdl") = weapon_arcwelder : "Arcwelder" []
// weapon_builder
@PointClass base(Weapon) studio("models/weapons/w_human_combatshield.mdl") = weapon_combat_shield : "Human combative shield weapon" []
@PointClass base(Weapon) studio("models/weapons/w_alien_combatshield.mdl") = weapon_combat_shield_alien : "Alien combative shield weapon" []
@PointClass base(Weapon) studio("models/weapons/w_combat_burstrifle.mdl") = weapon_combat_burstrifle : "Burst rifle & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_grenade_antipersonnel.mdl") = weapon_combat_grenade : "Combo shield & grenade weapon" [] // VXP: TODO: May be w_grenade
@PointClass base(Weapon) studio("models/weapons/w_grenade.mdl") = weapon_combat_grenade_emp : "Combo shield & grenade weapon" [] // VXP: TODO: May be w_grenade
@PointClass base(Weapon) studio("models/weapons/w_laserrifle.mdl") = weapon_combat_laserrifle : "Laser Rifle & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_human_plasmarifle.mdl") = weapon_combat_plasmarifle : "Laser Rifle & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_alien_plasmarifle.mdl") = weapon_combat_plasmarifle_alien : "Chargeable Plasma & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_combat_plasmagrenadelauncher.mdl") = weapon_combat_plasmagrenadelauncher : "Burst rifle & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_combat_shotgun.mdl") = weapon_combat_shotgun : "Shotgun & Shield combo" []
@PointClass base(Weapon) studio("models/weapons/w_drainbeam.mdl") = weapon_drainbeam : "Sapper's draining beam" []
@PointClass base(Weapon) studio("models/weapons/w_flamethrower.mdl") = weapon_flame_thrower : "Flamethrower" []
@PointClass base(Weapon) studio("models/weapons/w_flamethrower_gas_can.mdl") = weapon_gas_can : "Flamethrower's Gas Can" []
@PointClass base(Weapon) studio("models/weapons/w_grenade_rocket.mdl") = weapon_grenade_rocket : "Rockets (Weapon)" []
@PointClass base(Weapon) studio("models/weapons/w_harpoon.mdl") = weapon_harpoon : "Harpoon" [] // VXP: TODO: Take from HL2: LC
@PointClass base(Weapon) studio("models/weapons/w_limpetmine.mdl") = weapon_limpetmine : "Combination limpet mine & laser designator weapon" [] // VXP: TODO: Take from HL2: LC
@PointClass base(Weapon) studio("models/weapons/w_minigun.mdl") = weapon_minigun : "Minigun" []
@PointClass base(Weapon) studio("models/weapons/w_pistol.mdl") = weapon_objectselection : "This is a 'weapon' that's used to put objects into the client's weapon selection" []
@PointClass base(Weapon) studio("models/objects/obj_antimortar.mdl") = weapon_obj_empgenerator : "Combat object weapon for the EMP Generator" [] // VXP: FIXME: World model might be wrong!
@PointClass base(Weapon) studio("models/props/common/holo_banner/holo_banner.mdl") = weapon_obj_rallyflag : "Combat object weapon for the Rally Flag" []
@PointClass base(Weapon) studio("models/weapons/w_plasmarifle.mdl") = weapon_plasmarifle : "Medic's plasma rifle" [] // VXP: FIXME: w_human_plasmarifle maybe?
@PointClass base(Weapon) studio("models/weapons/w_repairgun.mdl") = weapon_repairgun : "The Medic's Medikit weapon" []
@PointClass base(Weapon) studio("models/weapons/w_rocket_launcher.mdl") = weapon_rocket_launcher : "Rocket Launcher (Weapon)" []
@PointClass base(Weapon) studio("models/weapons/w_projected_shield.mdl") = weapon_shield : "The Escort's Shield weapon" []
@PointClass base(Weapon) studio("models/weapons/w_grenade_shield.mdl") = weapon_shield_grenade : "The shield grenade weapon" []
@PointClass base(Weapon) studio("models/weapons/w_pistol.mdl") = weapon_twohandedcontainer : "Support weapon and weapons contained within it" []
