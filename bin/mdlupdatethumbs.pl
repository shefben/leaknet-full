sub ProcessMDL
{
	local( $filename ) = shift;
	local( $thumbfilename ) = $filename;
	local( $tgafilename ) = $filename;
	$thumbfilename =~ s/\.mdl$/.jpg/gi || die;
	$tgafilename =~ s/\.mdl$/.tga/gi || die;
	print "Building thumbnail $thumbfilename\n";

	local( $mtime );
	local( $mode );
	local( @statinfo ) = stat $filename;
	if( @statinfo )
	{
		$mtime = $statinfo[9];
		$mode = $statinfo[2];
	}
	else
	{
		die "CAN'T STAT $filename\n";
	}

	local( $thumbmtime );
	local( $thumbmode );
	@statinfo = stat $thumbfilename;
	if( @statinfo )
	{
		$thumbmtime = $statinfo[9];
		$thumbmode = $statinfo[2];
	}

	if( @statinfo && $thumbmtime >= $mtime )
	{
		return;
	}

	$cmd = "hlmv -screenshot $filename";
	$cmd =~ s,/,\\,;
	system $cmd;
	$cmd = "cjpeg $tgafilename $thumbfilename";
	system $cmd;

   unlink $tgafilename;
}

sub GetLocalDirList_File
{
	local( $filename ) = shift;
	local( $isdir ) = -d $filename;
	if( $isdir )
	{
		if( $filename =~ m/\/\.$/ ||		# "."
			$filename =~ m/\/\.\.$/ )	# ".."
		{
			return;
		}
	}

	if( $isdir )
	{
		GetLocalDirList_Dir( $filename );
	}
	if( $filename =~ m/\.mdl$/i )
	{
		&ProcessMDL( $filename );
	}
}

sub GetLocalDirList_Dir
{
	local( $dirname ) = shift;
	if( $dirname =~ m/\/\.$/ ||		# "."
		$dirname =~ m/\/\.\.$/ )	# ".."
	{
		return;
	}

	local( *SRCDIR );
	opendir SRCDIR, $dirname;
	local( @dir ) = readdir SRCDIR;
	closedir SRCDIR;

	local( $item );
	while( $item = shift @dir )
	{
		&GetLocalDirList_File( $dirname . "/" . $item );
	}
}

&GetLocalDirList_File( shift );

