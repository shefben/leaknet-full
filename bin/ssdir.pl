sub BackSlashToForwardSlash
{
	local( $in ) = shift;
	local( $out ) = $in;
	$out =~ s,\\,/,g;
	return $out;
}

sub ProcessDirectory
{
	local( $dir ) = shift;

	local( $exe ) = "ss dir \"$dir\"";
#	print $exe . "\n";
	local( @list ) = `$exe`;

	local( $item );
	while( $item = shift @list )
	{
		if( $item =~ m/No items found under/i )
		{
			last;
		}
		$item =~ s/ \(Cloaked\)//;
		if( $item =~ m/\:/ )
		{
			next;
		}
		if( $item =~ m/item\(s\)/ )
		{
			next;
		}
		if( $item =~ m/^\s*$/ )
		{
			next;
		}
		if( $item =~ s/^\$(.*)\n$/$1/ )
		{
#			print "dir: $dir/$item\n";
			&ProcessDirectory( "$dir/$item" );
		}
		else
		{
			$item =~ s/\n//;
			print "$dir/$item\n";
		}
	}
}

$baseDir = shift;		

if( !$baseDir )
{
	die "usage";
}

$baseDir = &BackSlashToForwardSlash( $baseDir );

&ProcessDirectory( $baseDir );

