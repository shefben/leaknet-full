:=============================================================================
: ssall, allows you to execute arbitrary source safe commands on our 
: executable set.  Run from your perforce source directory, so it can open the
: correct libs for edit as well in checkout mode.
:
: ssall checkout
: ssall checkin
: ssall undo
: etc, etc...
:=============================================================================

rem @ECHO OFF
set ssdir=\\Jeeves\HL2VSS\

:=============================================================================
: REPLACE THIS BIT WITH THE DIRECTORY SS.EXE RESIDES IN.
:=============================================================================
rem set ssexe=C:\Program Files\Microsoft Visual Studio\Common\VSS\win32\ss
set ssexe=ss

:=============================================================================
: REPLACE robin WITH YOUR VSS USERNAME (often not nessecary)
:=============================================================================
: set ssuser=nick

if "%1" == "" goto usage

set OPT=
if "%1" == "checkin" set OPT=-C-

"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/bsppack.dll 
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/tier0.dll 
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/bspzip.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/dbg.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/engine.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/FileSystem_Stdio.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/hlds.dat
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/hlds.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/hlfaceposer.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/hlmv.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/launcher.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/materialsystem.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/platform.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/shaderapidx8.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/shaderapidx9.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/shaderapiempty.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/studiomdl.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/StudioRender.Dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/unitlib.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vbsp.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vbspinfo.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vgui2.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vguimatsurface.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vmtedit.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vphysics.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vrad.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vrad.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vstdlib.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vtex.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vview.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vvis.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/wc.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/hl2.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/hl2.dat
"%ssexe%" %1 -GF -GWR %OPT%    $/TF2/release/dev/tf2.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/TF2/release/dev/tf2.dat
"%ssexe%" %1 -GF -GWR %OPT%    $/hl1ports/release/dev/hl1/bin/server.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/hl1ports/release/dev/hl1/bin/client.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/TF2/release/dev/tf2/bin/server.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/TF2/release/dev/tf2/bin/client.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/hl2/release/dev/hl2/bin/gameUI.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/hl2/bin/server.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/hl2/bin/client.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/height2normal.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/newdat.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/playback.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vtex.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vtf2tga.exe
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vvis.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vvis.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/FileSystem_Steam.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/bin/vaudio_miles.dll
"%ssexe%" %1 -GF -GWR %OPT%    $/HL2/release/dev/platform/servers/serverbrowser.dll


:Handle Perforce stuff on checkout only
if "%1" NEQ "checkout" goto end

rem p4 edit lib\public\*.lib

goto end

:usage
ECHO usage:
ECHO    ssall checkout 
ECHO    ssall checkin
ECHO    ssall undo

:end